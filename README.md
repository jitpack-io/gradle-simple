# gradle-simple

[![Release](https://img.shields.io/github/release/jitpack/gradle-simple.svg?label=maven version)](https://jitpack.io/#jitpack/gradle-simple)

Example Gradle project producing a single jar. Uses the `maven` plugin to publish the jar to the local repository.

[https://jitpack.io/#jitpack/gradle-simple/1.0](https://jitpack.io/#jitpack/gradle-simple/1.0)



